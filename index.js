const express = require('express');
const puppeteer = require('puppeteer');
const axios = require('axios');
const parser = require('xml2json');

const app = express();

app.get('/count', async (req, res) => {
    const cssSelector = req.query.cssSelector;
    const sitemapUrl = req.query.sitemapUrl;

    const response = await axios.get(sitemapUrl);
    const sitemap = JSON.parse(parser.toJson(response.data));
    const urls = sitemap.urlset.url;

    let elementCount = 0;

    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    for (const url of urls) {
        const location = url.loc;
        
        if (location.includes('.pdf')) {
            console.log(`Skipping: ${location}`);
            continue;
        };
        
        try {
            console.log(`Checking: ${location}`);
            await page.goto(url.loc, { waitUntil: 'networkidle0' });
            const length = await page.evaluate(cssSelector => document.querySelectorAll(cssSelector).length, cssSelector)
            console.log(`Elements found: ${length}`);
            
            elementCount += length;
            console.log(`Element total found: ${elementCount}`)
        } catch(err) {
            console.log(err.message);
        }

        console.log('\n');
    }

    await page.close();
    await browser.close();

    res.status(200).json({
        elementCount
    });
});

// start server
app.listen(3000);
