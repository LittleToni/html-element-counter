# HTML Element Counter

Counts the occurrence of the given element at the urls provided by a sitemap. 

## System Requirements

- nodejs

## Installation

Run `npm install` to get started.

## Examples & Usage

Run `node index.js` to start the service.

## API

- GET /count (sitemapUrl, cssSelector)
